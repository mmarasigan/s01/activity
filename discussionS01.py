# [Section] Commnents
# Comments


#[Section] Python Syntax
# Hello word in Python
print('Hellow World!')

#[Section] Indentation
#Where in other programming languages the indentation in code is for readability only, the indentation in Python is ver importan
# In Python, indentation is used to indicate a block of code.

#Similar to JS, there is no need to end statemenbts with semicolons

#[Sections] Variable
#Variables are the container of data
#In Python, a variable is declare by stating the variable name and assigning a value using equality symbol.

#[Section] Naming Convertion
# The terminology used for variable name is identifier
#All identifiers should begin with a letter (A to Z or a to z), dollar sign or an underscore.
#after the first character, identifiter can have any combination of characters
#Unlike Javascript that uses the camel casing, Python uses the snake case convetion for variables as defined in the PEP (python enhancement proposal)
#Most importantly, identifiers are case sensitive


age = 35
middle_initial ="C"
#Python allows assigning of values to multiple variable in one line.

name1,name2,name3,name4 = "John","Paul","George","Ringo"

print(name1)

# [Section] Data types


#Alphanumeric and symbols
full_name ="John Doe"
secret_code = "Pa$$word"

# 2. Number(int,float,complex) - for integers, decimal and complex number
num_of_days = 365 #integer
pi_approx = 3.1475 #float


#3. boolean (bool) - for truth values
#Boolean values in Python starts with upper case

isLearning = True
isDifficult =False

#[Section] using varialbles
#Just like in JS variable are used by simple calling the name of the identifier

#print('My Name is' + ' ' + full_name)
print(type(full_name))
print(full_name + ' ' + secret_code)


#[typcasting]
#age variable converted to string using str function
print('My age is ' + str(age))

#variable converted to string using str function
print(int(3.15))


#another way to avoid type error in printing without the use of typecasting 
#f-strings

print(f"Hi my name is {full_name} and my age is {age}.")

num1 =4
num1+=3
print(num1)




